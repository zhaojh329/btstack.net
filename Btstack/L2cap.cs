﻿using System;
using System.Runtime.InteropServices;

namespace Btstack
{
    public partial class Bluetooth
    {
        [DllImport("btstack-c.dll", EntryPoint = "l2cap_init")]
        private static extern void L2capInit();

        [DllImport("btstack-c.dll", EntryPoint = "l2cap_register_packet_handler")]
        private static extern void L2capRegisterPacketHandler(BtstackPacketHandlerType handler);

        [DllImport("btstack-c.dll", EntryPoint = "l2cap_le_register_service")]
        private static extern byte L2capLeRegisterService(BtstackPacketHandlerType handle, ushort psm, GapSecurityLevel SecurityLevel);

        private static ushort L2capEventConnectionParameterUpdateResponseGetResult(byte[] packet)
        {
            return BitConverter.ToUInt16(packet, 4);
        }

        private static ushort L2capEventLeIncomingConnectionGetPsm(byte[] packet)
        {
            return BitConverter.ToUInt16(packet, 11);
        }

        private static ushort L2capEventLeIncomingConnectionGetLocalCid(byte[] packet)
        {
            return BitConverter.ToUInt16(packet, 13);
        }
    }
}
