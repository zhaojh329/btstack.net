﻿using System.Runtime.InteropServices;

namespace Btstack
{
    public partial class Bluetooth
    {
        [DllImport("btstack-c.dll", EntryPoint = "sm_init")]
        private static extern void SmInit();

        [DllImport("btstack-c.dll", EntryPoint = "sm_set_io_capabilities")]
        private static extern void SmSetIoCapabilities(IoCapability capability);
    }
}
